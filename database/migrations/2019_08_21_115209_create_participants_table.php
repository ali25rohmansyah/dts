<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->char('nik', 16)->unique();
            $table->char('ipk');
            $table->string('jk');
            $table->string('alamat');
            $table->string('ktp')->nullable();
            $table->string('dns')->nullable();
            $table->string('ijasah')->nullable();
            $table->string('jurusan');
            $table->string('status');
            $table->string('status_ktp')->nullable();
            $table->string('status_dns')->nullable();
            $table->string('status_ijasah')->nullable();
            $table->string('valid_by')->nullable();
            $table->string('approved_by')->nullable();
            $table->string('valid_date')->nullable();
            $table->string('approved_date')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}

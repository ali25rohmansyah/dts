<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public $primaryKey = 'id_artikel';

    protected $fillable = ['judul', 'isi', 'label', 'author', 'thumbnail'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

<?php

namespace App\Http\Controllers;

use App\Participant;
use App\User;
use Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Nullable;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = DB::table('admins')
        ->where('username', session()->get('admin'))
        ->get();
        
        if(count($admin)>0){
            if ($admin[0]->level == '1') {
                $participant = DB::table('participants')
                ->join('users', 'users.nik', '=', 'participants.nik')
                ->where('status', 'Confirmed')
                ->orWhere('status', 'Complete')
                ->orderBy('status', 'asc')
                ->get();
            } else if($admin[0]->level == '2') {
                $participant = DB::table('participants')
                ->join('users', 'users.nik', '=', 'participants.nik')
                ->where('status', 'Complete')
                ->orWhere('status', 'Approved')
                ->orWhere('status', 'Confirmed')
                ->orderBy('status', 'desc')
                ->get();
            }
        }else{
            return redirect()->back();
        }

        $rekam = DB::table('participants')
        ->Join('users', 'users.nik', '=', 'participants.nik')
        ->where('sc', '1')
        ->orderBy('status', 'asc')
        ->get();

        return view('admin.participant.index',['participant' => $participant,'admin' =>$admin, 'rekam' =>$rekam]);
    }

    public function detail(Participant $participant)
    {
        $nik = $participant->nik;
        $participants = DB::table('participants')
        ->join('users', 'users.nik', '=', 'participants.nik')
        ->where('participants.nik', $nik)   
        ->get();

        // return $participant;
        return view('admin.participant.detail', ['participant' => $participants]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.participant.add_participants');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'no_hp' => 'required','numeric',
            'ipk' => 'required|numeric|between:0,4.00'
        ]);

        $ekstensi_diperbolehkan	= array('png', 'PNG', 'Png','jpg', 'JPG', 'Jpg', 'jpeg', 'Jpeg', 'JPEG', 'pdf', 'Pdf', 'PDF');
        $ktp = $request->file('ktp');
        $ktpnama =  $request->nik."_Ktp.".$ktp->getClientOriginalExtension();
        $ktpekstensi = $ktp->getClientOriginalExtension();
        $ktpukuran = $ktp->getSize();
        $ktp_tmp = $ktp->getRealPath();

        $dns = $request->file('dns');
        $dnsnama = $request->nik."_Dns.".$dns->getClientOriginalExtension();
        $dnsekstensi = $dns->getClientOriginalExtension();
        $dnsukuran = $dns->getSize();
        $dns_tmp = $dns->getRealPath();

        $ijasah = $request->file('ijasah');
        $ijasahnama = $request->nik."_Ijasah.".$ijasah->getClientOriginalExtension();
        $ijasahekstensi = $ijasah->getClientOriginalExtension();
        $ijasahukuran = $ijasah->getSize();
        $ijasah_tmp = $ijasah->getRealPath();
       
        $user = User::where('nik', '=',  $request->nik)->first();
        $user->name = $request->nama;
        $user->no_hp = $request->no_hp;
        $user->nama_universitas = $request->nama_universitas;
        $user->semester = $request->semester;

        $user->save();

        if(in_array($ktpekstensi, $ekstensi_diperbolehkan) === true && in_array($dnsekstensi, $ekstensi_diperbolehkan) === true && in_array($ijasahekstensi, $ekstensi_diperbolehkan) === true){
            if($ktpukuran < 1044070 && $dnsukuran < 1044070 && $ijasahukuran < 1044070){			
                move_uploaded_file($ktp_tmp, 'ckeditor/kcfinder/upload/images/'.$ktpnama);
                move_uploaded_file($dns_tmp, 'ckeditor/kcfinder/upload/images/'.$dnsnama);
                move_uploaded_file($ijasah_tmp, 'ckeditor/kcfinder/upload/images/'.$ijasahnama);

                if((file_exists('ckeditor/kcfinder/upload/images/'.$ktpnama))&&(file_exists('ckeditor/kcfinder/upload/images/'.$dnsnama))&&(file_exists('ckeditor/kcfinder/upload/images/'.$ijasahnama))){
                    Participant::create([
                        'nik'=> $request->nik,
                        'ipk'=> $request->ipk,
                        'jk'=> $request->gender,
                        'alamat'=>$request->alamat,
                        'ktp'=>$ktpnama,
                        'dns'=>$dnsnama,
                        'ijasah'=>$ijasahnama,
                        'status'=>'Confirmed',
                        'jurusan'=>$request->jurusan
                        
                    ]);

                    return redirect('/home');
                }else{
                    return redirect('/home')->with('message', 'Upload file gagal, pastikan koneksi stabil');
                }
            }else{
                return redirect('/home')->with('message', 'Ukuran file atau gambar terlalu besar');
            }
        }else{
            return redirect('/home')->with('message', 'Untuk format gambar gunakan jpg/png dan untuk format file gunakan pdf');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function show(Participant $participant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function edit(Participant $participant)
    {
        $participants = DB::table('participants')
        ->join('users', 'users.nik', '=', 'participants.nik')
        ->where('participants.nik', $participant->nik)   
        ->get();
        
        return view('admin.participant.edit_participant', ['participant' => $participants]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Participant $participant)
    {
        $participant = $participant::find($participant->nik);

        $admin = DB::table('admins')
        ->where('username', session()->get('admin'))
        ->get();
        
        if(count($admin)>0){
            if ($admin[0]->level == '1') {
                $participant->valid_by = session()->get('name');
                $participant->valid_date = Carbon::now();
                if($participant->status == 'Confirmed'){
                    $status = 'Complete';
                    $participant->status_ktp = 'on';
                    $participant->status_dns = 'on';
                    $participant->status_ijasah = 'on';
                }elseif($participant->status == 'Complete'){
                    $status = 'Confirmed';
                    $participant->status_ktp = '';
                    $participant->status_dns = '';
                    $participant->status_ijasah = '';
                }
            } else if($admin[0]->level == '2') {
                $participant->approved_by = session()->get('name');
                $participant->approved_date = Carbon::now();
                if($participant->status == 'Complete'){
                    $status = 'Approved';
                    $participant->status_ktp = 'on';
                    $participant->status_dns = 'on';
                    $participant->status_ijasah = 'on';
                }elseif($participant->status == 'Approved'){
                    $status = 'Complete';
                    $participant->status_ktp = 'on';
                    $participant->status_dns = 'on';
                    $participant->status_ijasah = 'on';
                }
            }
        }else{
            return redirect()->back();
        }
        
        $participant->status = $status;
        $participant->save();

        return redirect('/peserta');
    }

    public function updateData(Request $request, Participant $participant){
        $request->validate([
            'no_hp' => 'required|numeric',
            'ipk' => 'required|numeric|between:0,4.00'
        ]);

        $participant = $participant::find($participant->nik);
        $participant->ipk = $request->ipk;
        $participant->jk = $request->jk;
        $participant->alamat = $request->alamat;
        
        $participant->save();

        $user = User::where('nik', '=',  $participant->nik)->first();
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->no_hp = $request->no_hp;
        $user->nama_universitas = $request->nama_universitas;
        $user->semester = $request->semester;
        $user->tema = $request->tema;

        if (!is_null($request->password)) {
            $request->validate([
                'password' => 'string|min:8',
            ]);
            $user->password = Hash::make($request->password);
        }
        $user->save();

        $ekstensi_diperbolehkan	= array('png', 'PNG', 'Png','jpg', 'JPG', 'Jpg', 'jpeg', 'Jpeg', 'JPEG', 'pdf', 'Pdf', 'PDF');

        if (!is_null($request->ktp)) {
            $ktp = $request->ktp;
            $ktpnama =  $participant->nik."_Ktp.".$ktp->getClientOriginalExtension();
            $ktpekstensi = $ktp->getClientOriginalExtension();
            $ktpukuran = $ktp->getSize();
            $ktp_tmp = $ktp->getRealPath();

            if(in_array($ktpekstensi, $ekstensi_diperbolehkan) === true){
                if($ktpukuran < 1044070){

                    $path = "ckeditor/kcfinder/upload/images/".$participant->ktp;
                    @unlink($path);
                    
                    move_uploaded_file($ktp_tmp, 'ckeditor/kcfinder/upload/images/'.$ktpnama);
                    $ktp = $participant::find($participant->nik);
                    $ktp->ktp = $ktpnama;
                    $ktp->save();
                }else{
                    return back()->with('message', 'Ukuran file atau gambar ktp terlalu besar');
                }
            }else{
                return back()->with('message', 'Untuk format gambar gunakan jpg / png / jpeg dan untuk format file gunakan pdf');
            }
        }

        if (!is_null($request->dns)) {
            $dns = $request->file('dns');
            $dnsekstensi = $dns->getClientOriginalExtension();
            $dnsnama = $participant->nik."_Dns.".$dnsekstensi;
            $dnsukuran = $dns->getSize();
            $dns_tmp = $dns->getRealPath();

            if(in_array($dnsekstensi, $ekstensi_diperbolehkan) === true){
                if($dnsukuran < 1044070){

                    $path = "ckeditor/kcfinder/upload/images/".$participant->dns;
                    @unlink($path);

                    move_uploaded_file($dns_tmp, 'ckeditor/kcfinder/upload/images/'.$dnsnama);
                    $dns = $participant::find($participant->nik);
                    $dns->dns = $dnsnama;
                    $dns->save();
                }else{
                    return back()->with('message', 'Ukuran file atau gambar ktp terlalu besar');
                }
            }else{
                return back()->with('message', 'Untuk format gambar gunakan jpg / png / jpeg dan untuk format file gunakan pdf');
            }
        }

        if (!is_null($request->ijasah)) {
            $ijasah = $request->ijasah;
            $ijasahnama = $participant->nik."_Ijasah.".$ijasah->getClientOriginalExtension();
            $ijasahekstensi = $ijasah->getClientOriginalExtension();
            $ijasahukuran = $ijasah->getSize();
            $ijasah_tmp = $ijasah->getRealPath();

            if(in_array($ijasahekstensi, $ekstensi_diperbolehkan) === true){
                if($ijasahukuran < 1044070){
                    
                    $path = "ckeditor/kcfinder/upload/images/".$participant->ijasah;
                    @unlink($path);

                    move_uploaded_file($ijasah_tmp, 'ckeditor/kcfinder/upload/images/'.$ijasahnama);                    
                    $ijasah = $participant::find($participant->nik);
                    $ijasah->ijasah = $ijasahnama;
                    $ijasah->save();
                }else{
                    return back()->with('message', 'Ukuran file atau gambar ktp terlalu besar');
                }
            }else{
                return back()->with('message', 'Untuk format gambar gunakan jpg / png / jpeg dan untuk format file gunakan pdf');
            }
        }

        $admin = DB::table('admins')
        ->where('username', session()->get('admin'))
        ->get();
        
        $participant = DB::table('participants')
        ->join('users', 'users.nik', '=', 'participants.nik')
        ->where('status', 'Complete')
        ->orWhere('status', 'Approved')
        ->orWhere('status', 'Confirmed')
        ->orderBy('status', 'desc')
        ->get();


        return view('admin.participant.index',['participant' => $participant,'admin' =>$admin]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Participant $participant)
    {
        Participant::find($participant->nik)->delete();

            $path = "ckeditor/kcfinder/upload/images/".$participant->ktp;
            @unlink($path);

            $path = "ckeditor/kcfinder/upload/images/".$participant->dns;
            @unlink($path);

            $path = "ckeditor/kcfinder/upload/images/".$participant->ijasah;
            @unlink($path);

        return redirect('/peserta');
    }

    public function download(Participant $participant)
    {

        if($_GET['type'] == 'ktp'){
            $type = $participant->ktp;
        }elseif($_GET['type'] == 'dns'){
            $type = $participant->dns;
        }elseif($_GET['type'] == 'ijasah'){
            $type = $participant->ijasah;
        }
        
        $file_path = 'ckeditor/kcfinder/upload/images/'.$type;
        return response()->download($file_path);
    }

    public function unverified()
    {
        $users = DB::table('users')
        ->whereNull('email_verified_at')
        ->get();
        
        return view('admin.participant.unverified',['users' => $users]);
    }
    public function verified()
    {
        $users = DB::table('users')
        ->whereNotNull('email_verified_at')
        ->get();
        
        return view('admin.participant.verified',['users' => $users]);
    }
    public function updateunverified(User $user)
    {
        $user = User::where('nik', '=',  $user->nik)->first();
        $user->email_verified_at = Carbon::now();

        $user->save();  

        return redirect('/peserta/unverified');
    }
    public function destroyunverified(User $user)
    {
        User::find($user->id)->delete();
        return back();
    }
    public function destroyverified(User $user)
    {
        User::find($user->id)->delete();
        return back();
    }

    public function storeadmin(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users',
            'nik' => 'required|unique:users|unique:participants|digits:16',
            'no_hp' => 'required|numeric',
            'ipk' => 'required|numeric|between:0,4.00',
            'password' => 'required|min:8'
        ]);

        $ktpnama = '';
        $dnsnama = '';
        $ijasahnama = '';
        $ekstensi_diperbolehkan	= array('png', 'PNG', 'Png','jpg', 'JPG', 'Jpg', 'jpeg', 'Jpeg', 'JPEG', 'pdf', 'Pdf', 'PDF');
        
        if (!is_null($request->ktp)) {
            $ktp = $request->file('ktp');
            $ktpnama =  $request->nik."_Ktp.".$ktp->getClientOriginalExtension();
            $ktpekstensi = $ktp->getClientOriginalExtension();
            $ktpukuran = $ktp->getSize();
            $ktp_tmp = $ktp->getRealPath();

            if(in_array($ktpekstensi, $ekstensi_diperbolehkan) === true ){
                if($ktpukuran < 1044070){			
                    move_uploaded_file($ktp_tmp, 'ckeditor/kcfinder/upload/images/'.$ktpnama);

                }
            }
        }
        
        if(!is_null($request->ktp)){
            $dns = $request->file('dns');
            $dnsnama = $request->nik."_Dns.".$dns->getClientOriginalExtension();
            $dnsekstensi = $dns->getClientOriginalExtension();
            $dnsukuran = $dns->getSize();
            $dns_tmp = $dns->getRealPath();

            if(in_array($dnsekstensi, $ekstensi_diperbolehkan) === true ){
                if($dnsukuran < 1044070){			
                    move_uploaded_file($dns_tmp, 'ckeditor/kcfinder/upload/images/'.$dnsnama);

                }
            }
        }

        if(!is_null($request->ijasah)){
            $ijasah = $request->file('ijasah');
            $ijasahnama = $request->nik."_Ijasah.".$ijasah->getClientOriginalExtension();
            $ijasahekstensi = $ijasah->getClientOriginalExtension();
            $ijasahukuran = $ijasah->getSize();
            $ijasah_tmp = $ijasah->getRealPath();

            if(in_array($ijasahekstensi, $ekstensi_diperbolehkan) === true ){
                if($ijasahukuran < 1044070){			
                    move_uploaded_file($ijasah_tmp, 'ckeditor/kcfinder/upload/images/'.$ijasahnama);

                }
            }
        }
       
        User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'email_verified_at'=>Carbon::now(),
            'password'=>$request->password,
            'create_at'=>Carbon::now(),
            'nik'=>$request->nik,
            'no_hp'=>$request->no_hp,
            'nama_universitas'=>$request->nama_universitas,
            'semester'=>$request->semester,
            'tema'=>$request->tema,
        ]);

        Participant::create([
            'nik'=> $request->nik,
            'ipk'=> $request->ipk,
            'jk'=> $request->gender,
            'alamat'=>$request->alamat,
            'ktp'=>$ktpnama,
            'dns'=>$dnsnama,
            'ijasah'=>$ijasahnama,
            'status'=>'Approved',
            'jurusan'=>$request->jurusan
        ]);

        return back()->with('status','Peserta berhasil di daftarkan');
    }

    public function updateDOc(Participant $participant ,Request $request){
        $participant = $participant::find($participant->nik);

        $participant->status = $request->status;
        $participant->status_ktp = $request->ktp;
        $participant->status_dns = $request->dns;
        $participant->status_ijasah = $request->ijasah;
        
        $participant->save();

        $admin = DB::table('admins')
        ->where('username', session()->get('admin'))
        ->get();
        
        if (session()->get('level') == '1') {
            $participant = DB::table('participants')
            ->join('users', 'users.nik', '=', 'participants.nik')
            ->where('status', 'Confirmed')
            ->orWhere('status', 'Complete')
            ->orderBy('status', 'asc')
            ->get();
            return view('admin.participant.index',['participant' => $participant,'admin' =>$admin]);

        } else if(session()->get('level') == '2') {
            $participant = DB::table('participants')
            ->join('users', 'users.nik', '=', 'participants.nik')
            ->where('status', 'Complete')
            ->orWhere('status', 'Approved')
            ->orWhere('status', 'Confirmed')
            ->orderBy('status', 'desc')
            ->get();
            return view('admin.participant.index',['participant' => $participant,'admin' =>$admin]);
        }
    }

    public function nComplete(Participant $participant, User $user){
        $participant = Participant::get();

        $users = DB::table('users')
        ->whereNotIn('nik', $participant->map->only(['nik']))
        ->get();

        return view('admin.participant.nComplete_participants', ['participant' => $users]);

    }
}

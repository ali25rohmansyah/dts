<?php

namespace App\Http\Controllers;

use App\Mail\sendEmail;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
    public function dashboard()
    {
        $participant = DB::table('participants')
        ->join('users', 'users.nik', '=', 'participants.nik')
        ->get();

        $machineLearning = DB::table('participants')
        ->join('users', 'users.nik', '=', 'participants.nik')
        ->where('users.tema', 'Machine Learning')   
        ->get();

        $cloudComputing = DB::table('participants')
        ->join('users', 'users.nik', '=', 'participants.nik')
        ->where('users.tema', 'Cloud Computing')   
        ->get();

        $user = DB::table('users')
        ->get();

        $requested = count($user) - count($participant) ;
        $confirmed = count($participant);
        $total = $requested + $confirmed;
        $ml = count($machineLearning) ;
        $cc = count($cloudComputing) ;

        return view('admin.dashboard')->with(['requested' => $requested, 'total' => $total, 'confirmed' => $confirmed, 'ml' => $ml, 'cc' => $cc]);
    }
    public function home()
    {
        $articles = DB::table('articles')
        ->join('admins', 'admins.username', '=', 'articles.author')
        ->limit(3)
        ->orderBy('created_at', 'desc')
        ->get();
        return view('home.index', ['artikel' => $articles]);
    }
    public function about()
    {
        return view('home.about');
    }
    public function pelatihanMl()
    {
        $instructors = DB::table('instructors')
        ->where('kategori', 'pengajar')
        ->get();

        $asistant = DB::table('instructors')
        ->where('kategori', 'pengajar pendamping')
        ->get();

        $sek = DB::table('instructors')
        ->where('kategori', 'sekretariat')
        ->get();

        $pesertaMl = DB::table('peserta_batch2s')
        ->where('tema', 'Machine Learning')
        ->get();
        
        return view('home.pelatihanMl', ['instructors' => $instructors,'asistant'=> $asistant,'sek' => $sek,'pesertaMl' => $pesertaMl]);
    }

    public function pelatihanCc()
    {
        $instructors = DB::table('instructors')
        ->where('kategori', 'pengajar')
        ->get();

        $asistant = DB::table('instructors')
        ->where('kategori', 'pengajar pendamping')
        ->get();

        $sek = DB::table('instructors')
        ->where('kategori', 'sekretariat')
        ->get();

        $pesertaCc = DB::table('peserta_batch2s')
        ->where('tema', 'Cloud Computing')
        ->get();
        
        return view('home.pelatihanCc', ['instructors' => $instructors,'asistant'=> $asistant,'sek' => $sek,'pesertaMl' => $pesertaCc]);
    }
    public function jadwal()
    {
        return view('home.jadwal');
    }
    public function news(articles $articles)
    {
        return view('home.news', ['$articles' => $articles]);
    }
    public function newss()
    {
         $articles = DB::table('articles')
        ->join('admins', 'admins.username', '=', 'articles.author')
        ->orderBy('created_at', 'desc')
        ->limit(4)
        ->get();

         $article = DB::table('articles')
        ->join('admins', 'admins.username', '=', 'articles.author')
        ->orderBy('created_at', 'desc')
        ->paginate(10);
        return view('home.news', ['articles' => $articles, 'article' => $article]);
    }
    public function speakers()
    {
        $instructors = DB::table('instructors')
        ->where('kategori', 'pengajar machine learning')
        ->get();

        $asistant = DB::table('instructors')
        ->where('kategori', 'pengajar pendamping')
        ->get();

        $sek = DB::table('instructors')
        ->where('kategori', 'sekretariat')
        ->get();
        return view('home.speakers', ['instructors' => $instructors,'asistant'=> $asistant,'sek' => $sek]);
    }

    public function speakers2()
    {
        $instructors = DB::table('instructors')
        ->where('kategori', 'pengajar cloud computing')
        ->get();

        $asistant = DB::table('instructors')
        ->where('kategori', 'pengajar pendamping')
        ->get();

        $sek = DB::table('instructors')
        ->where('kategori', 'sekretariat')
        ->get();
        return view('home.speakers2', ['instructors' => $instructors,'asistant'=> $asistant,'sek' => $sek]);
    }

    public function daftarpeserta()
    {
        $participants = DB::table('participants')
        ->paginate(28);
        return view('home.daftarpeserta',['participants' => $participants]);
    }
    public function maintenance()
    {
        return view('home.maintenance');
    }
    public function hakewa()
    {
       
        return view('home.hakewa');
    }
    public function tata()
    {
       
        return view('home.tata');
    }
    public function silabus()
    {
       
        return view('home.silabus');
    }
    public function contact()
    {
       
        return view('home.contact');
    }
    public function daftar()
    {
        $settings = DB::table('settings')->first();
        return view('home.daftar', ['settings' => $settings]);
    }

    public function sendEmail(Request $request)
    {
        
        try{
            Mail::send('email', ['isi' => $request->contact_textarea], function ($message) use ($request)
            {
                $message->subject($request->Subject);
                $message->from($request->Email, $request->Name);
                $message->to('dts.gunadarma.info@gmail.com');
 
            });
            return back()->with('status','Email Berhasil Terkirim');
        }
        catch (Exception $e){
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
        return $request;

    }

}

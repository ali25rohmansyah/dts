<?php

namespace App\Http\Controllers;

use App\Participant;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $nik = Auth::user()->nik;
        $participants = DB::table('participants')
        ->join('users', 'users.nik', '=', 'participants.nik')
        ->where('participants.nik', $nik)   
        ->first();

        if (Participant::where('nik', '=', Auth::user()->nik)->exists()) {
            $status = 'confirm';
         }else{
            $status = 'requested';             
         }
        return view('home',['participant' => $participants])->with('status', $status);
    }

    public function update(Participant $participant ,Request $request)
    {
        $participant = $participant::find($participant->nik);

        $participant->sc = '1';
        
        $participant->save();

        return redirect("/home");

    }
}

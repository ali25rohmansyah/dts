<?php

namespace App\Http\Controllers;

use App\Image;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\TryCatch;

class UploadController extends Controller
{
    public function index()
    {
        $Image = DB::table('images')
        ->paginate(12);
        return view('home.galery', ['Image' => $Image]);

    }
    public function create()
    {
        return view('admin.galery.dragdrop');

    }

    public function store(Request $request)
    {
        $ekstensi_diperbolehkan	= array('png', 'PNG', 'Png','jpg', 'JPG', 'Jpg', 'jpeg', 'Jpeg', 'JPEG');
        $file = $request->file('file');
        $file_tmp = $file->getRealPath();
        $ekstensi = $file->getClientOriginalExtension();
        $fileName = time() . '.' . $ekstensi;
        $ukuran = $file->getSize();

        if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
            if($ukuran < 1044070){			
                move_uploaded_file($file_tmp, 'uploads/'.$fileName);

                Image::create([
                    'caption'=> $request->caption,
                    'filename'=>$fileName
                    
                ]);

            }else{
                return back()->with('status', 'Ukuran gambar terlalu besar!!');
            }
        }else{
            return back()->with('status', 'Gambar yang diperbolehkan hanya Jpg, Jpeg dan Png');
        }

        return redirect('/drag-drop-images');
    }
    public function destroy(Request $request,Image $image)
    {
        $checked = $request->input('checked',[]);
        Image::whereIn("filename",$checked)->delete(); 

        foreach ($checked as $images) {
            $path = "uploads/".$images;
            @unlink($path);
           
        }
        
        $image = DB::table('images')
        ->paginate(10);

        return view('/admin/galery/delete', ['Image' => $image]);
    }
    public function delete(Image $image)
    {

        $image = DB::table('images')
        ->paginate(10);

        return view('/admin/galery/delete', ['Image' => $image]);
    }
}

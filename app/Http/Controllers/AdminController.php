<?php

namespace App\Http\Controllers;

use App\Admin;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = DB::table('admins')
        ->get();

        return view('/admin/admin/index',['admins' => $admin]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admin.add_admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'username' => 'unique:admins'
        ]);

        $ekstensi_diperbolehkan	= array('png', 'PNG', 'Png','jpg', 'JPG', 'Jpg', 'jpeg', 'Jpeg', 'JPEG');
        $file = $request->file('file');
        $nama = $file->getClientOriginalName();
        $ekstensi = $file->getClientOriginalExtension();
        $ukuran = $file->getSize();
        $file_tmp = $file->getRealPath();

        if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
            if($ukuran < 1044070){			
                move_uploaded_file($file_tmp, 'ckeditor/kcfinder/upload/images/'.$nama);

                Admin::create([
                    'username'=> $request->username,
                    'password'=> $request->password,
                    'nama'=> $request->nama,
                    'foto'=> $nama,
                    'level'=> $request->level
                    
                ]);
                return redirect('/adminC');
            }{
                return back()->with('status', 'Ukuran gambar terlalu besar!!');
            }
        }else{
            return back()->with('status', 'Gambar yang diperbolehkan hanya Jpg, Jpeg dan Png');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        return view('admin.admin.edit_admin', ['admin' => $admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $admin = $admin::find($admin->id);
        $admin->username = $request->username;
        $admin-> password = $request->password;
        $admin->nama = $request->nama;
        $admin->level = $request->level;
        
        if (is_null($request->file)) {
            
        } else {
            $ekstensi_diperbolehkan	= array('png', 'PNG', 'Png','jpg', 'JPG', 'Jpg', 'jpeg', 'Jpeg', 'JPEG');
            $file = $request->file('file');
            $nama = $file->getClientOriginalName();
            $ekstensi = $file->getClientOriginalExtension();
            $ukuran = $file->getSize();
            $file_tmp = $file->getRealPath();

            if(in_array($ekstensi, $ekstensi_diperbolehkan) == true){
                if($ukuran < 1044070){			
                    move_uploaded_file($file_tmp, 'ckeditor/kcfinder/upload/images/'.$nama);

                    $admin->foto = $nama;    
                }else{
                    return back()->with('status', 'Ukuran gambar terlalu besar!!');
                }
            }else{
                return back()->with('status', 'Gambar yang diperbolehkan hanya Jpg, Jpeg dan Png');
            }
        }

        $admin->save();

        return redirect('/adminC');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        Admin::find($admin->id)->delete();

        $path = "ckeditor/kcfinder/upload/images/".$admin->foto;
        @unlink($path);

        return redirect('/adminC');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function registration()
    {
        $settings = DB::table('settings')->first();

        return view('admin.setting.registration',['settings' => $settings]);
    }
    public function update(Request $request)
    {
        DB::table('settings')
        ->where('id','=','1')
        ->update(['status' => $request->setting]);

        return back();
    }
}

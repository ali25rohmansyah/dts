 // Set new default font family and font color to mimic Bootstrap's default styling
 Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
 Chart.defaults.global.defaultFontColor = '#858796';

 // Pie Chart daftar peserta
 var ctx = document.getElementById("myPieChart");
 var myPieChart = new Chart(ctx, {
 type: 'doughnut',
 data: {
     labels: ["Seluruh peserta", "Cloud computing", "Machine learning", "Belum isi dokument", "Sudah isi dokumen"],
     datasets: [{
     data: [10, 10, 10, 10, 10],
     backgroundColor: ['#1cc88a', '#e6e6fa', '#9933ff', '#aa1414', '#3b5998'],
     hoverBackgroundColor: ['#1cc88a', '#e6e6fa', '#9933ff', '#aa1414', '#3b5998'],
     hoverBorderColor: "rgba(234, 236, 244, 1)",
     }],
 },
 options: {
     maintainAspectRatio: false,
     tooltips: {
     backgroundColor: "rgb(255,255,255)",
     bodyFontColor: "#858796",
     borderColor: '#dddfeb',
     borderWidth: 1,
     xPadding: 15,
     yPadding: 15,
     displayColors: false,
     caretPadding: 10,
     },
     legend: {
     display: false
     },
     cutoutPercentage: 80,
 },
 });

// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable()
});

$(document).ready(function() {
  $('#nComplete').DataTable({
    dom: 'Bfrtipl',
    buttons: [
          'excelHtml5', 'csvHtml5'
    ]
  })
});

$(document).ready(function() {
  $('#participants').DataTable( {
    dom: 'Bfrtipl',
    // "dom": '<lf<t>ip>',
    // lengthMenu: [[ '10 rows', '25 rows', '50 rows', 'Show all' ]],
    buttons: [{
          extend: 'excelHtml5',
          exportOptions: {
            columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 15, 16, 17, 18]    
        }
      },{
          extend: 'csvHtml5',
          exportOptions: {
            columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 15, 16, 17, 18]   
        }
      },
    ],
    "columnDefs": [
      {
        targets: [1, 7, 8, 9, 15, 16, 17, 18],
        visible: false
    }],
  });
});

$(document).ready(function() {
  $('#rekam').DataTable( {
    dom: 'B',
    buttons: [{
          extend: 'excelHtml5', text: 'Rekam Jejak',  
      },
    ],
    "columnDefs": [
      {
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15],
        visible: false
    }],
  });
});


@extends('layouts.admin')

@section('title', 'Admin')

@section('page-heading', 'Admin')

@section('content')
    <div class="card shadow mb-4">

        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Add Admin Account</h6>
        </div>

        <div class="card-body">

            <!-- Awal Container -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        @if (session('status'))
                            <div class="alert alert-danger" role="alert">
                                {{session('status')}}
                            </div>
                        @endif
                    </div>
                </div>
                <!-- Awal Instructor -->
                <div class="row">
                    <div class="col-md-12">
 
                        <form action="/adminC" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            
                            <select name="level" style="width:193px" class="form-control form-group float-md-right" required>
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                            <div class="form-group">
                                <input id="username" type="text" name="username" class="form-control" placeholder="username" required autofocus>
                                @if ($errors->has('username'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" name="password" class="form-control" placeholder="password" required autofocus>
                            </div>
                            <div class="form-group">
                                <input type="text" name="nama" class="form-control" placeholder="Full Name" required autofocus>
                            </div>
                            <div class="form-group">
                                Profile Image : <input type="file" name="file" class="" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="add" value="Add" class="btn btn-success">
                            </div>
                        </form>
                     
                    </div>
                </div>	
                <!-- Akhir Instructor  -->

            </div>
            <!-- Akhir Container -->
        </div>
    </div>
@endsection
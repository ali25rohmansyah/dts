@extends('layouts.admin')

@section('title', 'Admin')

@section('content')
   <!-- Begin Page Content -->
   <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">All Admin Accounts</h1>
        <p class="mb-4">Terdiri dari 2 level. level 1 hanya bisa melakukan validasi, sedangkan level 2 bisa menggunakan semua fitur</p>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Admin</h6>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Nama</th>
                    <th>Level</th>
                    <th>Foto</th>
                    <th><i class="fas fa-fw fa-cog"></i></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Nama</th>
                    <th>Level</th>
                    <th>Foto</th>
                    <th><i class="fas fa-fw fa-cog"></i></th>
                  </tr>
                </tfoot>
                <tbody>
                  @foreach ($admins as $item)
                  <tr>
                      <td>{{$loop -> iteration}}</td>
                      <td>{{($item->username)}}</td>
                      <td>{{($item->password)}}</td>
                      <td>{{($item->nama)}}</td>
                      <td>{{($item->level)}}</td>                      
                      <td align="center"><img class="img img-fluid" src="{{('/ckeditor/kcfinder/upload/images/'.$item->foto)}}" width="32"></td>
                      <td align="center">
                          <a href="adminC/edit/{{$item->id}}" class="btn btn-primary btn-sm"><i class="fas fa-fw fa-edit"></i></a>
                          <a href="adminC/destroy/{{$item->id}}" class="btn btn-danger btn-sm" onclick="return confirm('Yakin ingin meghapus data ?')"><i class="fas fa-fw fa-trash"></i></a>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->

@endsection
@extends('layouts.admin')

@section('title', 'Images')

@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Images</h1>
        <p class="mb-4">Silahkan pilih untuk melakukan delete.</p>

        <div class="row">
            <div class="col-md-12">
                <!-- Awal Panel -->
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Delete Images</div>
                    </div>

                    <!-- Awal Panel Body -->
                    <div class="card-body">
                        <section class="body">
                            <section role="main" class="content-body" style="padding: 20px;">

                                <form method="get" action="/galery/destroy/">
                                    <div class="row">
                                        <button style="margin-bottom:20px" class="btn btn-danger" type="submit">Delete</button>                                    
                                    </div>

                                    <!-- Team member -->
                                    <div style="margin-bottom:20px" class="row">

                                        @foreach ($Image as $item)
                                            <div class="col-md-4">
                                                <div style="margin-top:12px" class="checkbox">
                                                    <label><input type="checkbox" value="{{$item->filename}}" name="checked[]">{{$item->caption}}</label><br />
                                                </div> 
                                                <div class="img-thumbnail">
                                                    <img style="width: 100%;
                                                    height: 15vw;
                                                    object-fit: cover;" src="{{asset('/uploads/'.$item->filename)}}" class="img-fluid">
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>  
                                    <!-- /Team member -->    
                                </form>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <?php
                                        // config
                                        $link_limit = 7;
                                        ?>
                    
                                        @if ($Image->lastPage() > 1)
                                        <nav aria-label="Page navigation example">
                                            <ul class="pagination">
                                                <li class="page-item {{ ($Image->currentPage() == 1) ? ' disabled' : '' }}">
                                                    <a class="page-link" href="{{ $Image->url(1) }}">First</a>
                                                    </li>
                                                @for ($i = 1; $i <= $Image->lastPage(); $i++)
                                                    <?php
                                                    $half_total_links = floor($link_limit / 2);
                                                    $from = $Image->currentPage() - $half_total_links;
                                                    $to = $Image->currentPage() + $half_total_links;
                                                    if ($Image->currentPage() < $half_total_links) {
                                                        $to += $half_total_links - $Image->currentPage();
                                                    }
                                                    if ($Image->lastPage() - $Image->currentPage() < $half_total_links) {
                                                        $from -= $half_total_links - ($Image->lastPage() - $Image->currentPage()) - 1;
                                                    }
                                                    ?>
                                                    @if ($from < $i && $i < $to)
                                                        <li class="page-item {{ ($Image->currentPage() == $i) ? ' active' : '' }}">
                                                            <a class="page-link" href="{{ $Image->url($i) }}">{{ $i }}</a>
                                                        </li>
                                                    @endif
                                                @endfor
                                                <li class="page-item {{ ($Image->currentPage() == $Image->lastPage()) ? ' disabled' : '' }}">
                                                    <a class="page-link" href="{{ $Image->url($Image->lastPage()) }}">Last</a>
                                                </li>
                                            </ul>
                                        </nav>
                                        @endif
                                    </div>
                                </div>
                            </section>            
                        </section>
                    </div>
                    <!-- Akhir Panel Body -->

                </div>
                <!-- Akhir Panel -->
            </div>
        </div>
    </div>
    
@endsection
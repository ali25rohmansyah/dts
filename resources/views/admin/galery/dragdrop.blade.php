@extends('layouts.admin')

@section('title', 'Images')

@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Images</h1>
        <p class="mb-4">Gambar yang diperbolehkan hanya jpg, png dan jpeg.</p>

        <div class="row">
            <div class="col-md-12">
                <!-- Awal Panel -->
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Upload Images</div>
                    </div>

                    <!-- Awal Panel Body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                @if (session('status'))
                                    <div class="alert alert-danger" role="alert">
                                        {{session('status')}}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <form action="/drag-drop-images" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="text" name="caption" class="form-control" placeholder="Masukan Caption" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        Moment : <input type="file" name="file" class="" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="add" value="simpan" class="btn btn-success">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
@extends('layouts.admin')

@section('title', 'Participant')

@section('content')
   <!-- Begin Page Content -->
   <div class="container-fluid">

    <form action="/peserta/updateDoc/{{ $participant[0]->nik}}" method="get">
        <!-- Page Heading -->
        <div class="row">
            <div class="col">
                <h1 class="h3 mb-2 text-gray-800">Detail Participant</h1>
                <p class="mb-4"></p>
            </div>
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
              <div class="row"> 
                  <div class="col-md-6">
                        <h6 class="m-0 font-weight-bold text-primary">Identitas</h6>
                  </div>
                  <div class="col-md-6">
                        <h6 class="m-0 font-weight-bold text-primary">Dokumen</h6>
                  </div>
              </div>
          </div>
          <div class="card-body">
              <div class="row">
                <div class="col-md-2">
                        <p><b>NIK </b></p>
                        <p><b>Nama </b></p>
                        <p><b>E-Mail </b></p>
                        <p><b>No Hp </b></p>
                        <p><b>Nama Universitas </b></p>
                        <p><b>Jurusan </b></p>
                        <p><b>Semester </b></p>
                        <p><b>Ipk </b></p>
                        <p><b>Gender </b></p>
                        <p><b>Alamat </b></p>
                        <p><b>Tema </b></p>
                        <p><b>Valid By </b></p>
                        <p><b>Approved By </b></p>
                        <p><b>Valid Date </b></p>
                        <p><b>Approved Date </b></p>
                    </div>
    
                    <div class="col-md-4">
                        <p><b>: </b>{{$participant[0]->nik}} </p>
                        <p><b>: </b>{{$participant[0]->name}} </p>
                        <p><b>: </b>{{$participant[0]->email}} </p>
                        <p><b>: </b>{{$participant[0]->no_hp}} </p>
                        <p><b>: </b>{{$participant[0]->nama_universitas}} </p>
                        <p><b>: </b>{{$participant[0]->jurusan}} </p>
                        <p><b>: </b>{{$participant[0]->semester}} </p>
                        <p><b>: </b>{{$participant[0]->ipk}} </p>
                        <p><b>: </b>{{$participant[0]->jk}} </p>
                        <p><b>: </b>{{$participant[0]->alamat}} </p>
                        <p><b>: </b>{{$participant[0]->tema}} </p>
                        <p><b>: </b>{{$participant[0]->valid_by}} </p>
                        <p><b>: </b>{{$participant[0]->approved_by}} </p>
                        <p><b>: </b>{{$participant[0]->valid_date}} </p>
                        <p><b>: </b>{{$participant[0]->approved_date}} </p>
                    </div>

                    <div class="col-md-1">
                        <p><b>KTP</b></p>
                        <p><b>DNS</b></p>
                        <p><b>IJasah</b></p>
                        <p><b>Status</b></p>
                    </div>
                    <div class="col-md-4">
                         <p><b>: </b><a href="{{asset('/ckeditor/kcfinder/upload/images/'.$participant[0]->ktp)}}" target="_blank">{{$participant[0]->ktp}}</a> </p>
                        <p><b>: </b><a href="{{asset('/ckeditor/kcfinder/upload/images/'.$participant[0]->dns)}}" target="_blank">{{$participant[0]->dns}}</a> </p>
                        <p><b>: </b><a href="{{asset('/ckeditor/kcfinder/upload/images/'.$participant[0]->ijasah)}}" target="_blank">{{$participant[0]->ijasah}}</a> </p>
                       
                        <p><select name="status" class="form-control form-group" required autofocus>
                            <option value="Confirmed" {{ $participant[0]->status == 'Confirmed' ? 'selected' : ''}}>Confirmed</option>
                            <option value="Complete" {{ $participant[0]->status == 'Complete' ? 'selected' : ''}}>Complete Document</option>
                            @if (session()->get('level') == 2)
                                <option value="Approved" {{ $participant[0]->status == 'Approved' ? 'selected' : ''}}>Approved</option>
                            @endif
                        </select></p>

                        <div>
                            <div class="btn-lg btn-block">
                                <input type="submit" name="add" value="Save" class="btn btn-success btn-block">
                            </div>
                        </div>
                        
                    </div>

                    <div class="col-md-1">
                        <div class="form-check">
                            <p><input {{ $participant[0]->status_ktp == 'on' ? 'checked' : ''}} class="form-check-input position-static" name="ktp" type="checkbox"></p>
                        </div>
                        <div class="form-check">
                            <p><input {{ $participant[0]->status_dns == 'on' ? 'checked' : ''}} class="form-check-input position-static" name="dns" type="checkbox"></p>
                        </div>
                        <div class="form-check">
                            <p><input {{ $participant[0]->status_ijasah == 'on' ? 'checked' : ''}} class="form-check-input position-static" name="ijasah" type="checkbox"></p>
                        </div>
                    </div>
              </div>
          </div>
        </div>

        </form>
      </div>
      <!-- /.container-fluid -->

@endsection
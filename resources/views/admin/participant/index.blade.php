@extends('layouts.admin')

@section('title', 'Participant')

@section('content')
   <!-- Begin Page Content -->
   <div class="container-fluid">

      <!-- Page Heading -->
      <div class="row">
        <div class="col">
            <h1 class="h3 mb-2 text-gray-800">All Participants</h1>
            <p class="mb-4">Klik Pada Gambar Untuk Zoom</p>
        </div>
      </div>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Participants</h6>
        </div>
        <div class="card-body">
          
          <div class="table-responsive">

            <div style="margin-bottom:12px" class="div">
                <table style="display:none" class="table table-bordered" id="rekam" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>NIK</th>
                      <th>Nama</th>
                      <th>Universitas</th>
                      <th>Jurusan</th>
                      <th>Semester</th>
                      <th>Ipk</th>
                      <th>Email</th>
                      <th>No Hp</th>
                      <th>JK</th>
                      <th>Tema</th>
                      <th>Status</th>
                      <th>KTP</th>
                      <th>DNS</th>
                      <th>Ijasah</th>
                      <th>Alamat</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach ($rekam as $item)
                          <tr>
                              <td class="align-middle">{{$loop -> iteration}}</td>
                              <td class="align-middle">{{($item->nik)}}</td>
                              <td class="align-middle">{{($item->name)}}</td>
                              <td class="align-middle">{{($item->nama_universitas)}}</td>
                              <td class="align-middle">{{($item->jurusan)}}</td>
                              <td class="align-middle">{{($item->semester)}}</td>
                              <td class="align-middle">{{($item->ipk)}}</td>
                              <td class="align-middle">{{($item->email)}}</td>
                              <td class="align-middle">{{($item->no_hp)}}</td>
                              <td class="align-middle">{{($item->jk)}}</td>
                              <td class="align-middle">{{($item->tema)}}</td>
                                @if ($item->status == 'Confirmed')
                                  <td class="align-middle">
                                      <div class="badge badge-warning">{{($item->status)}}</div>
                                  </td>
                                @endif
                                @if ($item->status == 'Complete')
                                  <td class="align-middle">
                                      <div class="badge badge-success">{{($item->status)}}</div>
                                  </td>
                                @endif
                                @if ($item->status == 'Approved')
                                  <td class="align-middle">
                                    <div class="badge badge-primary">{{($item->status)}}</div>
                                  </td>
                                @endif
                              @if (($item->status_ktp) == 'on' )
                                <td class="align-middle">1</td>
                              @else
                                <td class="align-middle">0</td>
                              @endif
    
                              @if (($item->status_dns) == 'on' )
                                <td class="align-middle">1</td>
                              @else
                                <td class="align-middle">0</td>
                              @endif
    
                              @if (($item->status_ijasah) == 'on' )
                                <td class="align-middle">1</td>
                              @else
                                <td class="align-middle">0</td>
                              @endif
                              <td class="align-middle">{{($item->alamat)}}</td>
    
                          </tr>
                      @endforeach
                  </tbody>
                </table>
            </div>

            <table class="table table-bordered" id="participants" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Universitas</th>
                  <th>Jurusan</th>
                  <th>Semester</th>
                  <th>Ipk</th>
                  <th>Email</th>
                  <th>No Hp</th>
                  <th>JK</th>
                  <th>Tema</th>
                  <th>KTP</th>
                  <th>DNS</th>
                  <th>Ijasah</th>
                  <th>Status</th>
                  <th>KTP</th>
                  <th>DNS</th>
                  <th>Ijasah</th>
                  <th>Alamat</th>
                  <th><i class="fas fa-fw fa-cog"></i></th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                    <th>No</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Universitas</th>
                    <th>Jurusan</th>
                    <th>Semester</th>
                    <th>Ipk</th>
                    <th>Email</th>
                    <th>No Hp</th>
                    <th>JK</th>
                    <th>Tema</th>
                    <th>KTP</th>
                    <th>DNS</th>
                    <th>Ijasah</th>
                    <th>Status</th>
                    <th>KTP</th>
                    <th>DNS</th>
                    <th>Ijasah</th>
                    <th>Alamat</th>
                    <th><i class="fas fa-fw fa-cog"></i></th>
                </tr>
              </tfoot>
              <tbody>
                  @foreach ($participant as $item)
                      <tr>
                          <td class="align-middle">{{$loop -> iteration}}</td>
                          <td class="align-middle">{{($item->nik)}}</td>
                          <td class="align-middle">{{($item->name)}}</td>
                          <td class="align-middle">{{($item->nama_universitas)}}</td>
                          <td class="align-middle">{{($item->jurusan)}}</td>
                          <td class="align-middle">{{($item->semester)}}</td>
                          <td class="align-middle">{{($item->ipk)}}</td>
                          <td class="align-middle">{{($item->email)}}</td>
                          <td class="align-middle">{{($item->no_hp)}}</td>
                          <td class="align-middle">{{($item->jk)}}</td>
                          <td class="align-middle">{{($item->tema)}}</td>
                          <td class="align-middle" align="center">
                            @if ($item->ktp == '')
                                
                            @else
                              <a href="{{asset('/ckeditor/kcfinder/upload/images/'.$item->ktp)}}" target="_blank">Lihat</a>                                
                            @endif
                          </td>
                          <td class="align-middle" align="center">
                            @if ($item->dns == '')
                              
                            @else
                              <a href="{{asset('/ckeditor/kcfinder/upload/images/'.$item->dns)}}" target="_blank">Lihat</a>
                            @endif
                          </td>
                          <td class="align-middle" align="center">
                            @if ($item->ijasah == '')
                              
                            @else
                              <a href="{{asset('/ckeditor/kcfinder/upload/images/'.$item->ijasah)}}" target="_blank">Lihat</a>
                            @endif
                          </td>
                            @if ($item->status == 'Confirmed')
                              <td class="align-middle">
                                  <div class="badge badge-warning">{{($item->status)}}</div>
                              </td>
                            @endif
                            @if ($item->status == 'Complete')
                              <td class="align-middle">
                                  <div class="badge badge-success">{{($item->status)}}</div>
                              </td>
                            @endif
                            @if ($item->status == 'Approved')
                              <td class="align-middle">
                                <div class="badge badge-primary">{{($item->status)}}</div>
                              </td>
                            @endif
                          @if (($item->status_ktp) == 'on' )
                            <td class="align-middle">1</td>
                          @else
                            <td class="align-middle">0</td>
                          @endif

                          @if (($item->status_dns) == 'on' )
                            <td class="align-middle">1</td>
                          @else
                            <td class="align-middle">0</td>
                          @endif

                          @if (($item->status_ijasah) == 'on' )
                            <td class="align-middle">1</td>
                          @else
                            <td class="align-middle">0</td>
                          @endif
                          <td class="align-middle">{{($item->alamat)}}</td>

                          <td class="align-middle" align="center">

                            @if ($admin[0]->level == '1')

                              <a class="btn btn-warning btn-circle btn-sm" Onclick="return confirm('Yakin ingin mengubah status ?')" href="/peserta/update/{{$item->nik}}"><i class="fas fa-random"></i></a>

                            @else

                              @if ($item->status == 'Complete' || $item->status == 'Approved')
                                <a class="btn btn-warning btn-circle btn-sm" Onclick="return confirm('Yakin ingin mengubah status ?')" href="/peserta/update/{{$item->nik}}"><i class="fas fa-random"></i></a>                                                            
                              @endif

                            @endif
                            
                            <a class="btn btn-info btn-circle btn-sm" href="/peserta/detail/{{$item->nik}}"><i class="fas fa-eye"></i></a>
                            @if (session()->get('level') == '2')
                              <a class="btn btn-danger btn-circle btn-sm" Onclick="return confirm('Yakin ingin menghapus data ?')" href="/peserta/destroy/{{$item->nik}}"><i class="fas fa-trash"></i></a>
                              <a class="btn btn-primary btn-circle btn-sm" href="/peserta/edit/{{$item->nik}}"><i class="fas fa-edit"></i></a>
                            @endif
                            </td>

                      </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
    <!-- /.container-fluid -->

@endsection
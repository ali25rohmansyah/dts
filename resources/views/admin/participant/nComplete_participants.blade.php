@extends('layouts.admin')

@section('title', 'Participant')

@section('content')
   <!-- Begin Page Content -->
   <div class="container-fluid">

      <!-- Page Heading -->
      <div class="row">
        <div class="col">
            <h1 class="h3 mb-2 text-gray-800">Peserta yang belum isi dokumen</h1>
            <p class="mb-4">Daftar seluruh peserta yang belum isi dokumen</p>
        </div>
    
        <div class="col-auto">
            <a href="/peserta" class="btn btn-Secondary btn-icon-split">
                <span class="icon text-white-50">
                <i class="fas fa-arrow-left" ></i>
                </span>
                <span class="text">Back</span>
            </a>
        </div>
    </div>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Participants</h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="nComplete" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>NIK</th>
                  <th>Email</th>
                  <th>No Hp</th>
                  <th>Universitas</th>
                  <th>Semester</th>
                  <th>Tema</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Email</th>
                    <th>No Hp</th>
                    <th>Universitas</th>
                    <th>Semester</th>
                    <th>Tema</th>
                </tr>
              </tfoot>
              <tbody>
                  @foreach ($participant as $item)
                      <tr>
                          <td class="align-middle">{{$loop -> iteration}}</td>
                          <td class="align-middle">{{($item->name)}}</td>
                          <td class="align-middle">{{($item->nik)}}</td>
                          <td class="align-middle">{{($item->email)}}</td>
                          <td class="align-middle">{{($item->no_hp)}}</td>
                          <td class="align-middle">{{($item->nama_universitas)}}</td>
                          <td class="align-middle">{{($item->semester)}}</td>
                          <td class="align-middle">{{($item->tema)}}</td>
                      </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
    <!-- /.container-fluid -->

@endsection
@extends('layouts.admin')

@section('title', 'Participant')

@section('content')
   <!-- Begin Page Content -->
   <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col">
                <h1 class="h3 mb-2 text-gray-800">Verified Email</h1>
                <p class="mb-4">Semua email yang telah terveriikasi</p>
            </div>
        
            <div class="col-auto">
                <a href="/peserta" class="btn btn-Secondary btn-icon-split">
                    <span class="icon text-white-50">
                    <i class="fas fa-arrow-left" ></i>
                    </span>
                    <span class="text">Back</span>
                </a>
            </div>
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Verified Email</h6>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Email</th>
                    <th>Nik</th>
                    <th>No Hp</th>
                    <th>Nama Universitas</th>
                    <th>Semester</th>
                    <th>Tema</th>
                    <th><i class="fas fa-fw fa-cog"></i></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Email</th>
                    <th>Nik</th>
                    <th>No Hp</th>
                    <th>Nama Universitas</th>
                    <th>Semester</th>
                    <th>Tema</th>
                    <th><i class="fas fa-fw fa-cog"></i></th>
                  </tr>
                </tfoot>
                <tbody>
                    @foreach ($users as $item)
                        <tr>
                            <td class="align-middle">{{$loop -> iteration}}</td>
                            <td class="align-middle">{{($item->email)}}</td>
                            <td class="align-middle">{{($item->nik)}}</td>
                            <td class="align-middle">{{($item->no_hp)}}</td>
                            <td class="align-middle">{{($item->nama_universitas)}}</td>
                            <td class="align-middle">{{($item->semester)}}</td>
                            <td class="align-middle">{{($item->tema)}}</td>
                            <td class="align-middle" align="center">
                                <a class="btn btn-danger btn-circle btn-sm" Onclick="return confirm('Yakin ingin menghapus data ?')" href="/peserta/verified/destroy/{{$item->id}}"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->

@endsection
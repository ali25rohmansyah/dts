@extends('layouts.admin')

@section('title', 'Participant')

@section('page-heading', 'Participant')

@section('content')
    <div class="card shadow mb-4">

        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit Participant</h6>
        </div>

        <div class="card-body">

            <!-- Awal Container -->
            <div class="container-fluid">
                <form action="/peserta/updateData/{{$participant[0]->nik}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-12">
                        @if (session('message'))
                            <div class="alert alert-danger" role="alert">
                                {{session('message')}}
                            </div>
                        @endif
                    </div>
                </div>
                <!-- Ktp -->
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">NIK</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$participant[0]->nik}}</div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><i class="fas fa-upload"></i> Upload New KTP</div>
                                    <input type="file" id="ktp"  name="ktp" />
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><i class="fas fa-upload"></i> Upload New DNS</div>
                                    <input type="file" name="dns">
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><i class="fas fa-upload"></i> Upload New Ijasah</div>
                                    <input type="file" id="ijasah"  name="ijasah" />
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
                {{-- Akhir Ktp --}}

                <!-- Awal Participant -->
                <div class="row">
                    <div class="col-md-12">
 
                        <div class="row" >
                            <div class="col-md-3">
                                <p><label class="col-form-label"><b>Nama </b></label></p>
                                <p><label class="col-form-label"><b>E-Mail </b></label></p>
                                <p><label class="col-form-label"><b>Password (min 8 karakter) </b></label></p>
                                <p><label class="col-form-label"><b>No Hp </b></label></p>
                                <p><label class="col-form-label"><b>Nama Universitas </b></label></p>
                                <p><label class="col-form-label"><b>Semester </b></label></p>
                                <p><label class="col-form-label"><b>Ipk </b></label></p>
                                <p><label class="col-form-label"><b>Gender </b></label></p>
                                <p><label class="col-form-label"><b>Alamat </b></label></p>
                                <p><label class="col-form-label"><b>Tema </b></label></p>
                            </div>

                            <div class="col-md-9">
                                <div>
                                    <input type="hidden" name="id" class="form-control" value="{{$participant[0]->nik}}">
                                </div>
                                
                                <div class="form-group">
                                    <input type="text" name="nama" class="form-control" value="{{$participant[0]->name}}" required autofocus>
                                </div>

                                <div class="form-group">
                                    <input type="text" name="email" class="form-control" value="{{$participant[0]->email}}" required>
                                </div>

                                <div class="form-group">
                                    <input type="text" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Kosongkan jika tidak ada perubahan">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <input type="text" name="no_hp" class="form-control @error('no_hp') is-invalid @enderror" value="{{$participant[0]->no_hp}}" required>
                                    @error('no_hp')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <input type="text" name="nama_universitas" class="form-control" value="{{$participant[0]->nama_universitas}}" required>
                                </div>

                                <div class="form-group">
                                    <input type="text" name="semester" class="form-control" value="{{$participant[0]->semester}}" required>
                                </div>

                                <div class="form-group">
                                    <input type="text" name="ipk" class="form-control @error('ipk') is-invalid @enderror" value="{{$participant[0]->ipk}}" required>
                                    @error('ipk')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <input type="text" name="jk" class="form-control" value="{{$participant[0]->jk}}" required>
                                </div>

                                <div class="form-group">
                                    <input type="text" name="alamat" class="form-control" value="{{$participant[0]->alamat}}" required>
                                </div>

                                <div class="form-group">
                                    <select name="tema" class="form-control" required>
                                        <option value="Machine Learning" {{ $participant[0]->tema == 'Machine Learning' ? 'selected' : ''}}>Machine Learning</option>
                                        <option value="Cloud Computing" {{ $participant[0]->tema == 'Cloud Computing' ? 'selected' : ''}}>Cloud Computing</option>
                                    </select>
                                </div>
                            
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" name="edit" value="edit" class="btn btn-success float-md-right">
                        </div>
                     
                    </div>
                </div>	
                <!-- Akhir Instructor  -->

                </form>
            </div>
            <!-- Akhir Container -->
        </div>
    </div>
@endsection

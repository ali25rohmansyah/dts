@extends('layouts.admin')

@section('title', 'Participant')

@section('page-heading', 'Participant')

@section('content')
    <div class="card shadow mb-4">

        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Add Participant</h6>
        </div>

        <div class="card-body">

            <!-- Awal Container -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{session('status')}}
                            </div>
                        @endif
                    </div>
                </div>
                <!-- Awal Instructor -->
                <div class="row">
                    <div class="col-md-12">
                        
                            <form action="/peserta/admin" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                
                                <div class="form-group">

                                    <label class="col-form-label text-md-right">Tema <span>*</span></label>
                                    <select name="tema" class="form-control" required autofocus>
                                        <option value="Machine Learning">Machine Learning</option>
                                        <option value="Cloud Computing">Cloud Computing</option>
                                    </select>

                                </div>

                                <div class="form-group">

                                    <label class="col-form-label text-md-right">Nomor Induk Kependudukan(KTP) <span>*</span></label>
                                    <input type="text" name="nik" class="form-control @error('nik') is-invalid @enderror" required >
                                    
                                    @error('nik')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group">

                                    <label class="col-form-label text-md-right">Alamat Email <span>*</span></label>
                                    <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" required >
                                    
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    
                                </div>
                                <div class="form-group">

                                    <label class="col-form-label text-md-right">Password (Minimal 8 karakter) <span>*</span></label>
                                    <input type="text" name="password" class="form-control @error('password') is-invalid @enderror">
                                    
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group">

                                    <label class="col-form-label text-md-right">Nama Lengkap <span>*</span></label>
                                    <input type="text" name="name" class="form-control" required>
                                    
                                </div>
                                <div class="form-group">

                                    <label class="col-form-label text-md-right">No Handphone <span>*</span></label>
                                    <input type="text" name="no_hp" class="form-control @error('no_hp') is-invalid @enderror" required >
                                    
                                    @error('no_hp')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">

                                    <label class="col-form-label text-md-right">Nama Universitas</label>
                                    <input type="text" name="nama_universitas" class="form-control">
                                    
                                </div>
                                <div class="form-group">

                                    <label class="col-form-label text-md-right">Semester (Jika sudah lulu beri angka 0)</label>
                                    <input type="text" name="semester" class="form-control">
                                    
                                </div>
                                <div class="form-group">

                                    <label class="col-form-label text-md-right">Jurusan Kuliah</label>
                                    <input type="text" name="jurusan" class="form-control  @error('jurusan') is-invalid @enderror">

                                    @error('jurusan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
    
                                </div>
                                <div class="form-group">

                                    <label class="col-form-label text-md-right">IPK <span>*</span></label>
                                    <input type="text" name="ipk" class="form-control @error('ipk') is-invalid @enderror">
                                    
                                    @error('ipk')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">

                                    <label class="col-form-label text-md-right">Jenis Kelamin <span>*</span></label>
                                    <div class="form-radio">
                                        <input type="radio" name="gender" value="male" checked> Pria
                                        <input type="radio" name="gender" value="female"> Wanita
                                    </div>
                                    
                                </div>
                                <div class="form-group">

                                    <label class="col-form-label text-md-right">Alamat <span>*</span></label>
                                    <textarea type="text" name="alamat" class="form-control" required></textarea>
                                    
                                </div>

                                <div class="form-group">
                                    <label class="col-form-label text-md-right">Upload File (Untuk nama file yang diupload menggunakan format NIK_Ktp / NIK_Dns / NIK_Ijasah) <span>*</span></label>
                                    @if (session('message'))
                                        <div class="alert alert-danger" role="alert">
                                            {{session('message')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">Scan Ktp <br> (ex : 1234567891012131_Ktp)</div>
                                        <div class="col-md-9">
                                            : <input type="file" name="ktp" class="" value="ktp">
                                        </div>
                                            
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3"> DNS Semmester Terakhir <br> (ex : 1234567891012131_Dns)</div>
                                        <div class="col-md-9">
                                            : <input type="file" name="dns" class="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">Scan Ijasah/Sk Skripsi <br> (ex : 1234567891012131_Ijasah)</div>
                                            <div class="col-md-9">
                                                : <input type="file" name="ijasah" class="">
                                            </div>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <input type="submit" name="add" value="Submit" class="btn btn-success">
                                </div>
                            </form>
                     
                    </div>
                </div>	
                <!-- Akhir Instructor  -->

            </div>
            <!-- Akhir Container -->
        </div>
    </div>
@endsection
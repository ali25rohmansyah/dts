@extends('layouts.admin')

@section('title', 'setting')

@section('content')
   <!-- Begin Page Content -->
   <div class="container-fluid">

      <!-- Page Heading -->
      <div class="row">
        <div class="col">
            <h1 class="h3 mb-2 text-gray-800">Registration Setting</h1>
            <p class="mb-4">Untuk Mengatur pendaftaran</p>
        </div>
      </div>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Setting</h6>
        </div>
        <div class="card-body">
          <form action="/registration/update" method="get">
            {{ csrf_field() }}
            <select name="setting" class="custom-select form-group">
              <option value="1" {{($settings->status == 1)?'selected': ''  }} >Coming Soon</option>
              <option value="2" {{($settings->status == 2)?'selected': ''  }} >Opened</option>
              <option value="3" {{($settings->status == 3)?'selected': ''  }} >Closed Down</option>
            </select>
            <div class="form-group">
                <input type="submit" name="save" value="Save" class="btn btn-success">
            </div>
          </form>
        </div>
      </div>

    </div>
    <!-- /.container-fluid -->

@endsection
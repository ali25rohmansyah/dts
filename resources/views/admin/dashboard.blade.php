@extends('layouts.admin')

@section('title', 'Dashboard')

@section('page-heading', 'Dashboard')

@section('content')
    <!-- Chart -->
    <div class="row">
        <div class="col-md-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Statistik Peserta DTS 2019</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-bar">
                        <canvas id="myBarChart"></canvas>
                    </div>
                    <hr>
                    <b>Jumlah : </b><br>
                    <div style="margin-top:12px" class="row">
                        <div class="col-md-4">
                            <p>Belum isi dokumen</p>
                        </div>
                        <div class="col-md-6">
                            <p>: {{$requested}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p>Sudah isi dokumen</p>
                        </div>
                        <div class="col-md-6">
                            <p>: {{$confirmed}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p>Akun terdaftar</p>
                        </div>
                        <div class="col-md-6">
                            <p>: {{$total}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Statistik Pilihan Peserta DTS 2019</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="chart-pie pt-4">
                                <canvas id="myPieChart"></canvas>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="square" style="background-color: #e6e6fa;"></p>
                                </div>
                                <div class="col-md-9">
                                    <p>: Cloud computing</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="square" style="background-color: #9933ff;"></p>
                                </div>
                                <div class="col-md-9">
                                    <p>: Machine learning</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>
                        .square {
                            height: 25px;
                            width: 25px;}
                    </style>
                    <hr>
                    <b>Jumlah : </b><br>
                    <div style="margin-top:12px" class="row">
                        <div class="col-md-1">
                            <p class="square" style="background-color: #e6e6fa;"></p>
                        </div>
                        <div class="col-md-2">
                            <p>: {{$cc}}</p>
                        </div>
                        <div class="col-md-1">
                            <p class="square" style="background-color: #9933ff;"></p>
                        </div>
                        <div class="col-md-2">
                            <p>: {{$ml}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
@endsection

@section('chart')
    <script>
        // Set new default font family and font color to mimic Bootstrap's default styling
        Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        Chart.defaults.global.defaultFontColor = '#858796';

        // Pie Chart daftar peserta
        var ctx = document.getElementById("myPieChart");
        var myPieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ["Cloud computing", "Machine learning"],
            datasets: [{
            data: [{{$cc}}, {{$ml}}],
            backgroundColor: ['#e6e6fa', '#9933ff'],
            hoverBackgroundColor: ['#e6e6fa', '#9933ff'],
            hoverBorderColor: "rgba(234, 236, 244, 1)",
            }],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            caretPadding: 10,
            },
            legend: {
            display: false
            },
            cutoutPercentage: 80,
        },
        });
    </script>

<script>
        // Set new default font family and font color to mimic Bootstrap's default styling
        Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        Chart.defaults.global.defaultFontColor = '#858796';

        function number_format(number, decimals, dec_point, thousands_sep) {
        // *     example: number_format(1234.56, 2, ',', ' ');
        // *     return: '1 234,56'
        number = (number + '').replace(',', '').replace(' ', '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
        }

        // Bar Chart Example
        var ctx = document.getElementById("myBarChart");
        var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Belum isi dokumen", "Sudah isi dokumen", "Akun terdaftar"],
            datasets: [{
            label: "Jumlah",
            backgroundColor: "#4e73df",
            hoverBackgroundColor: "#2e59d9",
            borderColor: "#4e73df",
            data: [{{$requested}}, {{$confirmed}}, {{$total}}],
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
            padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
            }
            },
            scales: {
            xAxes: [{
                time: {
                unit: 'month'
                },
                gridLines: {
                display: false,
                drawBorder: false
                },
                ticks: {
                maxTicksLimit: 6
                },
                maxBarThickness: 25,
            }],
            yAxes: [{
                ticks: {
                min: 0,
                max: 300,
                maxTicksLimit: 5,
                padding: 10,
                // Include a dollar sign in the ticks
                callback: function(value, index, values) {
                    return  number_format(value);
                }
                },
                gridLines: {
                color: "rgb(234, 236, 244)",
                zeroLineColor: "rgb(234, 236, 244)",
                drawBorder: false,
                borderDash: [2],
                zeroLineBorderDash: [2]
                }
            }],
            },
            legend: {
            display: false
            },
            tooltips: {
            titleMarginBottom: 10,
            titleFontColor: '#6e707e',
            titleFontSize: 14,
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            caretPadding: 10,
            callbacks: {
                label: function(tooltipItem, chart) {
                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                }
            }
            },
        }
        });


    </script>
@endsection
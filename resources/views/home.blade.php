@extends('layouts.app')

@section('title', 'Profile')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('/css/contact.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/contact_responsive.css')}}">
@endsection
	
@section('home_background')
    <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{asset('images/baru/peserta.jpg')}}"data-speed="0.8"></div>
@endsection

@section('home')
    <div class="row">
        <div class="col">
            <div class="home_content d-flex flex-row align-items-end justify-content-start">
                <div class="current_page">Profile</div>
            </div>
        </div>
    </div>
@endsection

@section('intro')
    <div class="intro">
        <div class="container">
            <div class="row justify-content-center">
                @if ($status == 'requested')
                    <div class="col-md-12">
                        <div class="text-center">
                            <div class="alert alert-warning" role="alert">
                                Anda Belum Melengkapi Dokumen !!
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">Informasi Tentang Anda</div>
            
                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
    
                                <form action="/peserta" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    
                                    <div class="form-group">
    
                                        <label class="col-form-label text-md-right">Nomor Induk Kependudukan(KTP) <span>*</span></label>
                                        <input type="text" class="form-control" name="nik" value="{{{ Auth::user()->nik}}}" readonly="readonly" required>
    
                                    </div>
                                    <div class="form-group">
    
                                        <label class="col-form-label text-md-right">Alamat Email <span>*</span></label>
                                        <input type="text" class="form-control" value="{{{ Auth::user()->email}}}" readonly="readonly" required>
                                        
                                    </div>
                                    <div class="form-group">
    
                                        <label class="col-form-label text-md-right">Nama Lengkap <span>*</span></label>
                                        <input type="text" name="nama" class="form-control" value="{{{ Auth::user()->name}}}" required>
                                        
                                    </div>
                                    <div class="form-group">
    
                                        <label class="col-form-label text-md-right">No Handphone <span>*</span></label>
                                        <input type="text" name="no_hp" class="form-control @error('no_hp') is-invalid @enderror" value="{{{ Auth::user()->no_hp}}}" required >
                                        
                                        @error('no_hp')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
    
                                        <label class="col-form-label text-md-right">Nama Universitas <span>*</span></label>
                                        <input type="text" name="nama_universitas" class="form-control" value="{{{ Auth::user()->nama_universitas}}}" required>
                                        
                                    </div>
                                    <div class="form-group">
    
                                        <label class="col-form-label text-md-right">Semester <span>*</span></label>
                                        <input type="text" name="semester" class="form-control" value="{{{ Auth::user()->semester}}}" required>
                                        
                                    </div>
                                    <div class="form-group">

                                        <label class="col-form-label text-md-right">Jurusan Kuliah <span>*</span></label>
                                        <input type="text" name="jurusan" class="form-control" placeholder="Masukan jurusan anda" required autofocus>
        
                                    </div>
                                    <div class="form-group">
    
                                        <label class="col-form-label text-md-right">IPK<span>*</span></label>
                                        <input type="text" name="ipk" class="form-control @error('ipk') is-invalid @enderror" placeholder="Masukan IPK Anda" required>
                                        
                                        @error('ipk')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
    
                                        <label class="col-form-label text-md-right">Jenis Kelamin <span>*</span></label>
                                        <div class="form-radio">
                                            <input type="radio" name="gender" value="male" checked> Pria
                                            <input type="radio" name="gender" value="female"> Wanita
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
    
                                        <label class="col-form-label text-md-right">Alamat <span>*</span></label>
                                        <textarea type="text" name="alamat" class="form-control" placeholder="Masukan alamat lengkap anda" required></textarea>
                                        
                                    </div>
    
                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">Upload File (Untuk nama file yang diupload menggunakan format NIK_Ktp/NIK_Dns/NIK_Ijasah) <span>*</span></label>
                                        @if (session('message'))
                                            <div class="alert alert-danger" role="alert">
                                                {{session('message')}}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">Scan Ktp <br> (ex : 1234567891012131_Ktp)</div>
                                            <div class="col-md-9">
                                                : <input type="file" name="ktp" class="" value="ktp"  required>
                                            </div>
                                                
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3"> DNS Semmester Terakhir <br> (ex : 1234567891012131_Dns)</div>
                                            <div class="col-md-9">
                                                : <input type="file" name="dns" class=""  required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">Scan Ijasah/Sk Skripsi <br> (ex : 1234567891012131_Ijasah)</div>
                                                <div class="col-md-9">
                                                    : <input type="file" name="ijasah" class=""  required>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <input type="submit" name="add" value="Submit" class="btn btn-success">
                                    </div>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                @else
                    <div class="container" >
                        <div style="margin-bottom:10px" class="row">
                            <div class="col-md-12">
                                <div class="card text-center">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md 6">
                                            <p><a href="{{asset('images/baru/FINAL PERNYATAAN KOMITMEN PARTISIPASI DTS 2019_UG.pdf')}}"><b> Download Dokumen Pernyataan Komitmen</b></a></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><a href="{{asset('images/baru/Kelengkapan Dokumen Peserta DTS 2019 Batch 2.pdf')}}"><b> Download Dokumen Kelengkapan Peserta</b></a></p>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card text-center">
                                <div class="card-body">
                                    <p class="card-title"><b>{{$participant->tema}}</b></p>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div style="margin-top: 20px" class="row">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-header">
                                        <i class="fa fa-user"></i> Profil
                                    </div>
                                    <div class="card-body">
                                        <span class="card-title">Nama</span>
                                        <p class="card-text"> <i class="fa fa-user-md" ></i> {{$participant->name}}</p>
                                        <span class="card-title">Jenis Kelamin</span>
                                        <p class="card-text"> <i class="fa fa-venus-mars" ></i> {{$participant->jk}}</p>
                                        <span class="card-title">Alamat</span>
                                        <p class="card-text"> <i class="fa fa-map-marker" ></i> {{$participant->alamat}}</p>
                                        
                                    </div>
                                </div>

                                <div style="margin-top:20px; margin-bottom:20px" class="card">
                                    <div class="card-header">
                                        <i class="fa fa-graduation-cap" ></i> Pendidikan
                                    </div>
                                    <div class="card-body">
                                        <span class="card-title">Asal Universitas</span>
                                        <p class="card-text"> <i class="fa fa-university" ></i> {{$participant->nama_universitas}}</p>
                                        <span class="card-title">Semester</span>
                                        <p class="card-text"> <i class="fa fa-book" ></i> {{$participant->semester}}</p>
                                        <span class="card-title">IPK</span>
                                        <p class="card-text"> <i class="fa fa-star-half-o" ></i> {{$participant->ipk}}</p>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="card text-right">
                                    <div class="card-header">
                                        <i class="fa fa-address-card" ></i> NIK
                                    </div>
                                    <div class="card-body">
                                        <p class="card-text"> <i class="fa fa-address-book" ></i> {{$participant->nik}}</p>                                  
                                    </div>
                                </div>

                                <div style="margin-top:20px" class="card">
                                    <div class="card-header">
                                        <i class="fa fa-location-arrow" ></i> Kontak
                                    </div>
                                    <div class="card-body">
                                        <span class="card-title">No Handphone</span>
                                        <p class="card-text"> <i class="fa fa-phone" ></i> {{$participant->no_hp}}</p>                                    
                                        <span class="card-title">Email</span>
                                        <p class="card-text"> <i class="fa fa-envelope" ></i> {{$participant->email}}</p>                                    
                                    </div>
                                </div>
                                @if ($participant->status == 'Approved')
                                    <div style="margin-top:20px" class="card alert-success">
                                        <div class="card-header">
                                            <i class="fa fa-check-circle-o" ></i> Status
                                        </div>
                                        <div class="card-body">
                                            <p class="card-text"> <i class="fa fa-check" ></i> Diterima</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="pengumuman" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle"><p><b>Pengumuman!</b></p></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <p>
                    Seluruh peserta HARUS mensubmit dokumen sesuai dengan tema dan lokasi pelatihan dimana peserta 
                    dinyatakan diterima, 
                    dan memastikan dirinya terdaftar di website <b>digitalent.kominfo.go.id </b>dengan status "menunggu".
                    Tekan tombol OK yang akan membawa Anda ke halaman website <b>digitalent.kominfo.go.id </b>
                </p>
            </div>
            <div class="modal-footer">
            
                <form method="post" action="/sc/{{$participant->nik}}" id="submit">
                    {{ csrf_field() }}
                    <button type="submit" id="ok" name="ok" class="btn btn-primary">Ok</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('js')
    @if ($participant->sc == '')
        <script>
            $('#pengumuman').modal('show');
        </script>
    @endif
    <script>
    document.getElementById("ok").onclick = function() {
        window.open('https://digitalent.kominfo.go.id','_blank');
        document.getElementById("submit").submit();        
    return false;
    };
    </script>
    <script src="{{asset('/js/speakers.js')}}"></script>    
    <script src="{{asset('/js/contact.js')}}"></script> 
@endsection

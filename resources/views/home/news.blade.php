@extends('layouts.app')

@section('title', 'Berita')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('/css/news.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/contact.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/news_responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/responsive.css')}}">
@endsection
	
@section('home_background')
    <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{asset('images/baru/berita.jpg')}}"data-speed="0.8"></div>
@endsection
    
@section('home')
    <div class="row">
        <div class="col">
            <div class="home_content d-flex flex-row align-items-end justify-content-start">
                <div class="current_page">Berita</div>
                <div class="breadcrumbs ml-auto">
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li>Berita</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('intro')
    <div class="news">
        <div class="container">
            <div class="row">
                    <div class="col-lg-8">
                        <div class="news_items">	
                            
                            @foreach ($article as $item)													
                                <div class="news_item" id=''>
                                    <div class="news_image_container">
                                        <div class="news_image"><img src="{{asset('/ckeditor/kcfinder/upload/images/'.$item->thumbnail)}}"  alt=""></div>
                                    </div>

                                    <div class="news_body">
        
                                        <div class="news_title"><a href="#">{{($item->judul)}}</a></div>
        
                                        <div class="news_info">
                                            <ul>
                                                <!-- News Author -->
                                                <li>
                                                    <div class="news_author_image"><img src="{{asset('/ckeditor/kcfinder/upload/images/'.$item->foto)}}" alt=""></div>
                                                    <span>by <a>{{($item->nama)}}</a></span>
                                                </li>
                                                <!-- Label -->
                                                <li><span>in <a>{{($item->label)}}</a></span></li>
                                                <!-- Tanggal -->
                                                <li><a>{{($item->created_at)}}</a></li>
                                            </ul>
                                        </div>

                                        <div class="news_text">
                                            <?php
                                                $art = $item->isi;
                                            ?>
                                            <p>
                                                <?= $art ?>
                                            </p>
                                        </div>
        
                                    </div>

                                </div>
                            @endforeach

                            <!-- Pagination -->
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <?php
                                    // config
                                    $link_limit = 7;
                                    ?>
                
                                    @if ($article->lastPage() > 1)
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination">
                                            <li class="page-item {{ ($article->currentPage() == 1) ? ' disabled' : '' }}">
                                                <a class="page-link" href="{{ $article->url(1) }}">First</a>
                                                </li>
                                            @for ($i = 1; $i <= $article->lastPage(); $i++)
                                                <?php
                                                $half_total_links = floor($link_limit / 2);
                                                $from = $article->currentPage() - $half_total_links;
                                                $to = $article->currentPage() + $half_total_links;
                                                if ($article->currentPage() < $half_total_links) {
                                                    $to += $half_total_links - $article->currentPage();
                                                }
                                                if ($article->lastPage() - $article->currentPage() < $half_total_links) {
                                                    $from -= $half_total_links - ($article->lastPage() - $article->currentPage()) - 1;
                                                }
                                                ?>
                                                @if ($from < $i && $i < $to)
                                                    <li class="page-item {{ ($article->currentPage() == $i) ? ' active' : '' }}">
                                                        <a class="page-link" href="{{ $article->url($i) }}">{{ $i }}</a>
                                                    </li>
                                                @endif
                                            @endfor
                                            <li class="page-item {{ ($article->currentPage() == $article->lastPage()) ? ' disabled' : '' }}">
                                                <a class="page-link" href="{{ $article->url($article->lastPage()) }}">Last</a>
                                            </li>
                                        </ul>
                                    </nav>
                                    @endif
                                </div>
                            </div>
            
                        </div>
                    </div>
                

                    <!-- Sidebar -->
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="latest_posts">
                                <div class="latest_posts_title">Latest Posts</div>
                                <div class="latest_container">
                                    @foreach ($articles as $item)

                                        <!-- Latest -->
                                        <div class="latest d-flex flex-row align-items-start justify-content-start">
                                            <div>
                                                <div class="latest_image"><img src="{{asset('/ckeditor/kcfinder/upload/images/'.$item->thumbnail)}}" alt=""></div>
                                            </div>

                                            <div class="latest_content">

                                                <div class="latest_title"><a href="">{{($item->judul)}}</a></div>
                                                <div class="latest_date">{{($item->created_at)}}</div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
            
            </div>
        </div>
    </div>
@endsection
 
@section('js')
    <script src="{{asset('/js/news.js')}}"></script>   
@endsection
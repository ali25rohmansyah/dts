@extends('layouts.app')

@section('title', 'Pelaksanaan')

@section('css')

<link rel="stylesheet" type="text/css" href="{{asset('/css/speakers.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/css/contact.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/css/responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/css/contact_responsive.css')}}">
@endsection

@section('home_background')
<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="{{asset('images/baru/cloud_com.jpg')}}"
    data-speed="0.8"></div>
@endsection

@section('home')
<div class="row">
    <div class="col">
        <div class="home_content d-flex flex-row align-items-end justify-content-start">
            <div class="current_page">Pelatihan</div>
            <div class="breadcrumbs ml-auto">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li>Pelatihan</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('intro')
<div class="intro">
    <div class="container">
        <div class="row">
            <div class="col col-lg-12">
                <div class="cta_title text-center"><b>Cloud Computing</b></div>
                <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
            <li class="nav-item col-6 d-flex justify-content-center">
                <a class="nav-link d-flex justify-content-center" id="pills-home-tab" data-toggle="pill"
                    href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><p> <b>Batch 1</b> </p></a>
            </li>
            <li class="nav-item col-6 d-flex justify-content-center">
                <a class="nav-link d-flex justify-content-center active" id="pills-profile-tab" data-toggle="pill"
                    href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><p> <b>Batch 2</b> </p></a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            
            <div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="row">
                    <div class="col-md-2">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                            aria-orientation="vertical">
                            <a class="nav-link" id="v-pills-waktu-tab" data-toggle="pill" href="#v-pills-waktu"
                                role="tab" aria-controls="v-pills-waktu" aria-selected="true">Waktu</a>
                            <a class="nav-link  active" id="v-pills-peserta-tab" data-toggle="pill" href="#v-pills-peserta"
                                role="tab" aria-controls="v-pills-peserta" aria-selected="false">Peserta</a>
                            <a class="nav-link" id="v-pills-silabus-tab" data-toggle="pill" href="#v-pills-silabus"
                                role="tab" aria-controls="v-pills-silabus" aria-selected="false">Silabus</a>
                        </div>
                    </div>
                    <div class="col-md-10">

                        {{-- Awal Tab Content --}}
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade" id="v-pills-waktu" role="tabpanel"
                                aria-labelledby="v-pills-waktu-tab">
                                <p><b>Waktu Pelatihan</b><br></p>
                                <p>9 Juli 2019 sampai dengan 27 Agustus 2019</p>
                            </div>

                            <div class="tab-pane fade  show active" id="v-pills-peserta" role="tabpanel"
                                aria-labelledby="v-pills-peserta-tab">
                                <div class="elements_accordions_tabs">
                                    <center>
                                        <p><b>Pengumuman Hasil Seleksi Calon Peserta Program Fresh
                                                Graduate Academy DigitalTalent Scholarship Tahun 2019
                                                Kementerian Komunikasi dan Informatika Republik Indonesia</b><br></p>
                                    </center>
                                    <div class="row">
                                        <div class="col col-lg-12" id="tabs">
                                            <div class="tabs">
                                                <div class="tabs_container">

                                                    <div class="tab_panels">
                                                        <div class="tab_panel active">
                                                            <div class="tab_panel_content">
                                                                <div class="table-responsive">
                                                                    <table style="color:black; margin-top:12px"
                                                                        id="example" class="table table-bordered">
                                                                        <thead>
                                                                            {{-- <tr>
                                                                                <th scope="col">No.</th>
                                                                                <th scope="col">Penyelenggara</th>
                                                                                <th scope="col">Tema Pelatihan</th>
                                                                                <th scope="col">Nama Peserta</th>
                                                                                <th scope="col">Asal Universitas</th>
                                                                            </tr> --}}
                                                                            <tr>
                                                                                <th scope="col">No</th>
                                                                                <th scope="col">Nama</th>
                                                                                <th scope="col">Universitas</th>
                                                                                <th scope="col">Tema</th>
                                                                                <th scope="col">Gender</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @foreach ($pesertaMl as $item)
                                                                            <tr>
                                                                                {{-- @if ($item->email_verified_at<'2019-08-30 00:00:00')
                                                                                    <td>{{($loop -> iteration)}}</td>
                                                                                    <td>Universitas Gunadarma</td>
                                                                                    <td>{{($item->tema)}}</td>
                                                                                    <td>{{($item->name)}}</td>
                                                                                    <td>{{($item->nama_universitas)}}</td>
                                                                                @endif --}}
                                                                            </tr>
                                                                            @endforeach

                                                                        </tbody>
                                                                    </table>
                                                                    {{-- {{($peserta->links())}} --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="v-pills-silabus" role="tabpanel"
                                aria-labelledby="v-pills-silabus-tab">
                                <p><a href="{{asset('images/baru/silabus.pdf')}}"> Download silabus</a></p>
                            </div>
                            {{-- Akhir Tab Contetn --}}

                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">

                <div class="row">
                    <div class="col-md-2">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                            aria-orientation="vertical">
                            <a class="nav-link" id="v-pills-waktu-tab1" data-toggle="pill" href="#v-pills-waktu1"
                                role="tab" aria-controls="v-pills-waktu1" aria-selected="true">Waktu</a>
                            <a class="nav-link  active" id="v-pills-peserta-tab1" data-toggle="pill" href="#v-pills-peserta1"
                                role="tab" aria-controls="v-pills-peserta1" aria-selected="false">Peserta</a>
                            <a class="nav-link" id="v-pills-silabus-tab1" data-toggle="pill" href="#v-pills-silabus1"
                                role="tab" aria-controls="v-pills-silabus1" aria-selected="false">Silabus</a>
                        </div>
                    </div>
                    <div class="col-md-10">

                        {{-- Awal Tab Content --}}
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade" id="v-pills-waktu1" role="tabpanel"
                                aria-labelledby="v-pills-waktu-tab1">
                                <p><b>Waktu Pelatihan</b><br></p>
                                <p>22 Oktober 2019 - 14 November 2019</p>
                            </div>

                            <div class="tab-pane fade  show active" id="v-pills-peserta1" role="tabpanel"
                                aria-labelledby="v-pills-peserta-tab1">
                                <div class="elements_accordions_tabs">
                                    <center>
                                        <p><b>Pengumuman Daftar Peserta DigitalTalent
                                            Scholarship Batch 2 Tahun 2019
                                            Kementerian Komunikasi dan Informatika Republik
                                            Indonesia</b><br></p>
                                    </center>
                                    <div class="row">
                                        <div class="col col-lg-12" id="tabs">
                                            <div class="tabs">
                                                <div class="tabs_container">

                                                    <div class="tab_panels">
                                                        <div class="tab_panel active">
                                                            <div class="tab_panel_content">
                                                                <div class="table-responsive">
                                                                    <table style="color:black; margin-top:12px"
                                                                        id="example2" class="table table-bordered">
                                                                        <thead>
                                                                            {{-- <tr>
                                                                                <th scope="col">No.</th>
                                                                                <th scope="col">Penyelenggara</th>
                                                                                <th scope="col">Tema Pelatihan</th>
                                                                                <th scope="col">Nama Peserta</th>
                                                                                <th scope="col">Asal Universitas</th>
                                                                            </tr> --}}

                                                                            <tr>
                                                                                <th scope="col">No</th>
                                                                                <th scope="col">Nama</th>
                                                                                <th scope="col">Universitas</th>
                                                                                <th scope="col">Tema</th>
                                                                                <th scope="col">Gender</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @foreach ($pesertaMl as $item)
                                                                            <tr>
                                                                                {{-- @if ($item->email_verified_at>='2019-08-30 00:00:00')

                                                                                    <td>{{($loop -> iteration)}}</td>
                                                                                    <td>Universitas Gunadarma</td>
                                                                                    <td>{{($item->tema)}}</td>
                                                                                    <td>{{($item->name)}}</td>
                                                                                    <td>{{($item->nama_universitas)}}</td>

                                                                               @endif --}}

                                                                                    <td>{{($loop -> iteration)}}</td>
                                                                                    <td>{{$item->name}}</td>
                                                                                    <td>{{($item->universitas)}}</td>
                                                                                    <td>{{($item->tema)}}</td>
                                                                                    <td>{{($item->gender)}}</td>

                                                                            </tr>
                                                                            @endforeach

                                                                        </tbody>
                                                                    </table>
                                                                    {{-- {{($peserta->links())}} --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="v-pills-silabus1" role="tabpanel"
                                aria-labelledby="v-pills-silabus-tab1">
                                <p><a href="{{asset('images/baru/silabus cc.pdf')}}"> Download silabus</a></p>
                            </div>
                            {{-- Akhir Tab Contetn --}}

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
                <div class="intro_content d-flex flex-row flex-wrap align-items-start justify-content-between">
                    <div class="intro_body">
                        <p class="text-justify">

                            Pelatihan ini secara garis besar bertujuan untuk memberikan pemahaman dan pengalaman
                            kepada peserta dalam perancangan sistem dan aplikasi menggunakan Cloud Computing.
                            Peserta dibekali dengan teori dan praktik untuk memahami, menggunakan, serta
                            mengaplikasikan Cloud Computing sederhana.<br>
                            <br> Spesifikasi laptop dan aplikasi adalah : <br>
                            1. RAM min. 2GB.<br>
                            2. OS : Windows 7,8, atau 10, MAC OSX.<br>
                            3. Processor : 32/64-bit.<br>
                            4. Web browser : versi terbaru Microsoft Internet Explorer, Google Chrome, atau
                            Mozilla Firefox yang telah terinstall versi terbaru Java dan Flash Player

                        </p>
                        <p><b>Tempat Penyelengaraan</b></p>
                        <p>Universitas Gunadarma, Kampus J6 - Lantai 6
                            <br>Jl. Cikunir Raya No. 100 Jakamulya Bekasi Selatan
                            <br>Telp. 087788119994</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="{{asset('/js/contact.js')}}"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="http://malsup.github.com/jquery.media.js"></script>
<script type="text/javascript">
    $(function () {
        $('.media').media({ width: 868 });
    });
</script>
@endsection
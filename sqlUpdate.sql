CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL
); INSERT INTO `settings` (`id`, `status`) VALUES
(1, 2);

ALTER TABLE `participants` ADD `status_ktp` VARCHAR(191) NULL DEFAULT NULL AFTER `status`, 
ADD `status_dns` VARCHAR(191) NULL DEFAULT NULL AFTER `status_ktp`, 
ADD `status_ijasah` VARCHAR(191) NULL DEFAULT NULL AFTER `status_dns`;